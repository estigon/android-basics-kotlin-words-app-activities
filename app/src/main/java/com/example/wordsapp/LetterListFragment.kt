package com.example.wordsapp

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.wordsapp.databinding.FragmentLetterListBinding

class LetterListFragment : Fragment() {
    companion object{
        const val TAG = "LetterListFragment"
    }

    /*Why make it nullable? Because you can't inflate the layout until onCreateView() is called. There's a period of
    time in-between when the instance of LetterListFragment is created (when its lifecycle begins with onCreate()) and
    when this property is actually usable. Also keep in mind that fragments' views can be created and destroyed several
    times throughout the fragment's lifecycle. For this reason you also need to reset the value in another lifecycle method,
    onDestroyView().*/
    private var _binding: FragmentLetterListBinding? = null

    /*
    Because it's nullable, every time you access a property of _binding, (e.g. _binding?.someView) you need to include the ?
    for null safety. However, that doesn't mean you have to litter your code with question marks just because of one null
    value. If you're certain a value won't be null when you access it, you can append !! to its type name. Then you can
    access it like any other property, without the ? operator.*/

    //Here, get() means this property is "get-only". That means you can get the value, but once assigned (as it is here), you can't assign it to something else.
    private val binding get() = _binding!! //usamos esta variable para no tener que usar la anotacion de sefe null(_binding?) cada vez que usamos _binding
    private lateinit var recyclerView: RecyclerView
    private var isLinearLayoutManager = true

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(TAG, "onCreate")
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    //inflo el layout
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(TAG, "onCreateView")
        super.onCreateView(inflater, container, savedInstanceState)
        _binding = FragmentLetterListBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    //en este metodo tengo acceso a los elementos de mi vista
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.d(TAG, "onViewCreated")
        super.onViewCreated(view, savedInstanceState)
        recyclerView = binding.recyclerView
        chooseLayout()
    }

    // in onDestroyView(), reset the _binding property to null, as the view no longer exists.
    override fun onDestroyView() {
        Log.d(TAG, "onDestroyView")
        super.onDestroyView()
        _binding = null
    }

    //agregando el menu al fragment
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.layout_menu, menu)
        val layoutButton = menu.findItem(R.id.action_switch_layout)
        setIcon(layoutButton)
    }

    //agregando el icono
    private fun setIcon(menuItem: MenuItem?) {
        if (menuItem == null)
            return

        menuItem.icon =
            if (isLinearLayoutManager)
                //a diferencia de un activity un fragment no es un Context, asi que no podemos pasarle this
                //pero podemos pasarle this.requireContext() que nos devuelve el Context al que esta asociado el fragmento
                ContextCompat.getDrawable(this.requireContext(), R.drawable.ic_grid_layout)
            else ContextCompat.getDrawable(this.requireContext(), R.drawable.ic_linear_layout)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_switch_layout -> {
                // Sets isLinearLayoutManager (a Boolean) to the opposite value
                isLinearLayoutManager = !isLinearLayoutManager
                // Sets layout and icon
                chooseLayout()
                setIcon(item)

                return true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun chooseLayout() {
        if (isLinearLayoutManager) {
            recyclerView.layoutManager = LinearLayoutManager(this.requireContext())
        } else {
            recyclerView.layoutManager = GridLayoutManager(this.requireContext(), 4)
        }
        recyclerView.adapter = LetterAdapter()
    }
}